package com.example.alexb.phone;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.ListView;

import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    ArrayList<Contact> contactos ;
    private SwipeRefreshLayout swipe = null;
    String numero, mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contactos = new ArrayList<>();
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipe.setOnRefreshListener(this);


        contactList();
        final ListView linearLayout = findViewById(R.id.contactList);

        linearLayout.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                swipe.setEnabled(true);
            }
        });

        linearLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                numero = contactos.get(i).numero;
                PopupMenu popup = new PopupMenu(MainActivity.this,linearLayout);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        int id = menuItem.getItemId();

                        if (id == R.id.one){
                            llamar(numero);
                            return true;
                        }

                        if (id == R.id.two){
                            sendmesage(numero,"");
                            return true;
                        }

                        return false;
                    }
                });
                popup.show();
            }
        });

    }

    public void sendmesage(String tel, String mensage) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + tel));
        intent.putExtra(tel, mensage);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                startActivity(intent);
            else
                requestPermissions(new String[]{Manifest.permission.SEND_SMS}, 002);
        }

        else
            startActivity(intent);

    }
    public void contactList(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                setContactList();
            else
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 003);
        }

        else
            setContactList();
    }


    private void setContactList() {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    if (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Contact temp = new Contact(phoneNo,name);
                        contactos.add(temp);

                    }
                    pCur.close();
                }
            }
        }
        if(cur!=null){
            cur.close();
        }
        ListView linearLayout = findViewById(R.id.contactList);
        ArrayAdapter<Contact> arrayAdapter = new ArrayAdapter<Contact>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                contactos );
        linearLayout.setAdapter(arrayAdapter);
    }


    public int getContactsNum(){
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        return cur.getCount();
    }





    public void llamar(String tel)  {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + tel));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                startActivity(callIntent);
            else
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 001);
        }

            else
                startActivity(callIntent);

        }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        if(requestCode == 001){
            llamar(numero);

        }
        if(requestCode == 002) {
            sendmesage(numero, "");
        }

        if(requestCode == 003){
            setContactList();

        }




    }


    @Override
    public void onRefresh() {
        swipe.setRefreshing(true);
        Toast.makeText(this, "Tienes " + getContactsNum() +" Contactos.", Toast.LENGTH_SHORT).show();
        swipe.setRefreshing(false);
    }
}
