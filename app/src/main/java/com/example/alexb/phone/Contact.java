package com.example.alexb.phone;

/**
 * Created by alexb on 28/01/2018.
 */

public class Contact {
    String numero , nombre;

    @Override
    public String toString() {
        return
                nombre + " : " + numero;
    }

    public Contact(String numero, String nombre) {
        this.numero = numero;
        this.nombre = nombre;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
